document.addEventListener('DOMContentLoaded', () => {
  const breedsDropdown = document.getElementById('breeds');
  const dogImage = document.getElementById('dogImage');
  const loadBreedsButton = document.getElementById('loadBreedsButton');
  const showImageButton = document.getElementById('showImageButton');
  const clearButton = document.getElementById('clearButton');

  // Función para cargar las razas de perros
  function loadBreeds() {
    fetch('https://dog.ceo/api/breeds/list')
      .then(response => response.json())
      .then(data => {
        const breeds = data.message;
        populateBreedsDropdown(breeds);
      })
      .catch(error => console.error('Error al obtener la lista de razas:', error));
  }

  // Evento al clicar en el botón "Cargar Razas"
  loadBreedsButton.addEventListener('click', loadBreeds);

  // Evento al clicar en el botón "Mostrar Imagen"
  showImageButton.addEventListener('click', () => {
    const selectedBreed = breedsDropdown.value;
    fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
      .then(response => response.json())
      .then(data => {
        const imageUrl = data.message;
        dogImage.src = imageUrl;
      })
      .catch(error => console.error('Error al obtener la imagen del perro:', error));
  });

  // Evento al clicar en el botón "Limpiar"
  clearButton.addEventListener('click', () => {
    dogImage.src = '';
  });

  // Función para poblar el dropdown de razas de perros
  function populateBreedsDropdown(breeds) {
    breeds.forEach(breed => {
      const option = document.createElement('option');
      option.value = breed;
      option.textContent = breed;
      breedsDropdown.appendChild(option);
    });
  }
});
